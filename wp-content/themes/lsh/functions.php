<?php
/**
 * Lsh functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Lsh 
 * @since Inbar 1.0
 */

/**
 * Enqueues scripts and styles.
 *
 * @since Lsh 1.0
 */
function lsh_styles_and_scripts() {
    
       // Theme stylesheet.
	wp_enqueue_style( 'lsh-style', get_stylesheet_uri() );

	// Load other stylesheet.
	if( is_singular('post')){
	wp_enqueue_style( 'news-fonts', get_template_directory_uri() . '/assets/css/news-font.css', array(), '' );
	}
	wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/css/slick.css', array(), '' );
	wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/assets/css/slick-theme.css', array(), '' );
	wp_enqueue_style( 'styles', get_template_directory_uri() . '/assets/css/styles.css', array(), '' );
	wp_enqueue_style( 'mobile', get_template_directory_uri() . '/assets/css/mobile.css', array(), '' );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/assets/css/responsive.css', array(), '' );
	//effects css
	wp_enqueue_style( 'effects-vendor', get_template_directory_uri() . '/assets/css/effects-vendor.css', array(), '' );
	wp_enqueue_style( 'effects', get_template_directory_uri() . '/assets/css/effects.css', array(), '' );
	// Load the javascripts
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-2.8.3.min.js', array(), '', false);
    // Load the html5 shiv.
	 wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	 wp_script_add_data( 'html5', 'conditional', 'IE' );
	
	 wp_deregister_script('jquery');

	if( is_singular('projects') ){
	wp_enqueue_script( 'jquery-three', get_theme_file_uri( '/assets/js/vendor/jquery-3.1.0.min.js' ), array(), '1.0', true);
	}else{
	wp_enqueue_script( 'jquery-three', get_theme_file_uri( '/assets/js/vendor/jquery-3.1.0.min.js' ), array(), '1.0', false);
	}

	 wp_enqueue_script( 'slick', get_theme_file_uri( '/assets/js/slick.min.js' ), array(), '1.0', true );

	 if( !is_page_template(array('page-template/tpl-philosophy.php')) ){
	 wp_enqueue_script( 'respond', get_theme_file_uri( '/assets/js/respond.min.js' ), array(), '1.0', true );
	 wp_enqueue_script( 'main', get_theme_file_uri( '/assets/js/main.js' ), array(), '1.0', true );
	 wp_enqueue_script( 'effects-vendor', get_theme_file_uri( '/assets/js/effects-vendor.js' ), array(), '1.0', true );
	 wp_enqueue_script( 'fullpage', get_theme_file_uri( '/assets/js/jquery.fullpage.extensions.min.js' ), array(), '1.0', true );
	 }

	 if( is_page_template(array('page-template/tpl-home.php') ) ){
	 wp_enqueue_script( 'effect', get_theme_file_uri( '/assets/js/effects.js' ), array(), '1.0', true );
	 }

	 if( is_singular('projects')){
		 wp_enqueue_script('facncybox-js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js', array(), '1.0', true);
	 }
 }

add_action( 'wp_enqueue_scripts', 'lsh_styles_and_scripts' );

function prefix_add_footer_styles() {
	    wp_enqueue_style( 'fancybox-css',  'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css');
};
add_action( 'get_footer', 'prefix_add_footer_styles' );

//title tag enabled

add_action( 'after_setup_theme', 'inbar_enable_title' );

function inbar_enable_title() {

		add_theme_support( 'title-tag' );
}


//language switcher for lsh

function lsh_language_switcher() {

      $languages = icl_get_languages( 'skip_missing=0&orderby=KEY&order=DIR' );

      $la = '';

    if( !empty($languages) ){
     
	foreach ( $languages as $lang ) {

	    $la .= '<div class="t-symbol ' . ( $lang['active'] ? 'active' : ''  ) . '"><a href=' . $lang['url'] . '>' . $lang['translated_name'] . "</a></div>";
	 }

    }

     return $la;
 }


 /*
 * register header menu and footer menu
 */
 function lsh_init_menus() {

       register_nav_menus(
	       array(
	         'header-menu' => __( 'Header Menu' ),
		 'footer-menu' => __( 'Footer Menu' ),
	            )
          );

    }
 add_action( 'init', 'lsh_init_menus' );


/*
*  function for output header menu 
*/
 
 function lsh_header_menu_none( $menu_slug = 'header-menu' ){
 
	 global $post;  

	$menu_list = '<div class="navigation"><nav>';
	$menu = wp_get_nav_menu_object($menu_slug); 
	
	if( $menu ){
	    
		$menu_list .= '<ul>';
		$menu_items = wp_get_nav_menu_items($menu->term_id);
		
		foreach( $menu_items as $menu_item ) {
		
		    $class = $menu_item->object_id == $post->ID ? 'active' : '';
		    $menu_list .= '<li class="menu-item-'. $menu_item->object_id.' '.$class . '"><a href="' . $menu_item->url . '">' . $menu_item->title . '</a></li>' ."\n";

		}

	}	

	$menu_list .= '</ul></nav>'.lsh_language_switcher().'</div>'; 
 
     return $menu_list;
 }

 
/*
*  function for output header menu 
*/
 
 function lsh_header_menu( $menu_slug = 'header-menu' ){
 
	 global $post;  

	$menu_list = '<div class="navigation"><nav>';
	$menu = wp_get_nav_menu_object($menu_slug); 
    $count = 0;
	$submenu = false;
	$menu_list .= '<ul>';

	if( $menu ){
	    
		$menu_items = wp_get_nav_menu_items($menu->term_id);
		
		foreach( $menu_items as $menu_item ) {
		
		if( !$menu_item->menu_item_parent ){

		    $menu_list .= '<li>'. "\n";
		    $parent_id = $menu_item->ID;
		    $menu_list .= '<a href="' .$menu_item->url .'">' .$menu_item->title . '</a>'. "\n";
		 }

		 if ( $parent_id == $menu_item->menu_item_parent ) {
	
		     if ( !$submenu ) {
			 
			$submenu = true;
			$menu_list .= '<span class="readmore-ul"></span><ul>';
		     }

		     $menu_list .= '<li>'. "\n";
		     $menu_list .= '<a href="' . $menu_item->url . '">' . $menu_item->title . '</a>'."\n";
		     $menu_list .= '</li>'."\n";
                  
		    if ( !empty($menu_items[ $count+1 ]) && $menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ){
              $menu_list .= '</ul>' ."\n";
			 $submenu = false;
		      }

		 }


		  if ( !empty($menu_items[ $count+1 ]) && $menu_items[ $count + 1 ]->menu_item_parent != $parent_id ) { 
			    $submenu = false;
			    $menu_list .= '</li>'."\n";
		  }

		 $count++;
				
		}

	}	

	$menu_list .= '</ul></nav>'.lsh_language_switcher().'</div>'; 
 
     return $menu_list;
 }



/*
*  function for output footer menu 
*/

 function lsh_footer_menu( $menu_slug = 'footer-menu' ){

    $menu = wp_get_nav_menu_object($menu_slug); 
    $menu_list = '<div class="footer_row cfix">' . "\n";

	if( $menu ){

	    $menu_items = wp_get_nav_menu_items($menu->term_id);
	    //var_dump($menu_items);

	    $count = 0;
	    $submenu = false;
            $f = 1;
	    foreach( $menu_items as $menu_item ){

	    
		if( !$menu_item->menu_item_parent ){
		    $menu_list .= '<div class="footer_col">'. "\n";
		    $parent_id = $menu_item->ID;
		    $menu_list .= '<h4><a href="' .$menu_item->url .'">' .$menu_item->title . '</a></h4>'. "\n";
		 }

		 if ( $parent_id == $menu_item->menu_item_parent ) {
	
		     if ( !$submenu ) {
			 
			$submenu = true;
			$menu_list .= '<ul>';
		     }

		     $menu_list .= '<li>'. "\n";
		     $menu_list .= '<a href="' . $menu_item->url . '">' . $menu_item->title . '</a>'."\n";
		     $menu_list .= '</li>'."\n";
                  
		    if ( !empty($menu_items[ $count+1 ]) && $menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ){
                         $menu_list .= '</ul>' ."\n";
			 $submenu = false;
		      }

		 }


		  if ( !empty($menu_items[ $count+1 ]) && $menu_items[ $count + 1 ]->menu_item_parent != $parent_id ) { 
			    $submenu = false;
			    $menu_list .= '</div>'."\n";
			    $f+=1;
		  }

		 if( $f == 3 ){
		    
		     $menu_list .= '<div class="footer_col footer_col_two">
	    <a href="' . get_home_url() . '"><img src="' . get_template_directory_uri() .'/assets/images/footer-logo.png" alt="logo"></a>
    </div>'. "\n";
		     $f = -10;
 
		 }

		 $count++;

	    }
	}

   $menu_list .= '</div>';

return $menu_list;
 }


/*
 * create  new image styles for lsh theme
 */
function lsh_new_image_sizes(){

   if ( function_exists( 'add_image_size' ) ) { 

        add_image_size( 'home_header_slider_img', 1440, 810, array( 'center', 'center' ) );
	//lastest news style on homepage
        add_image_size( 'news_list_img', 269, 180, array( 'center', 'center' ) );
        add_image_size( 'news_list_img_news_page', 596, 296, array( 'center', 'center' ) );
        add_image_size( 'projects_taxonomy_list_img', 1440, 393, array( 'center', 'center' ) );

    }
 }
 add_action('after_setup_theme', 'lsh_new_image_sizes');
 
 
 //get total area of projects in one category

 function lsh_total_area_of_a_cat( $term_id ){
 
    $total = 0; 
    $args = array(
	    'post_type' => 'projects',
	    'post_status' => 'publish',
	    'orderby' => 'menu_order',
	    'order'   => 'ASC',
	    'tax_query' => array(
		    array(
		    'taxonomy' => 'types',
		    'field'    => 'term_id',
		    'terms'    => array($term_id ),
		    ),
	    ),

	    'posts_per_page'	=> -1
	);
   
    $query = new WP_Query( $args );

    if( $query->have_posts() ){

	while( $query->have_posts()){

	    $query->the_post();
	    
	    $total += get_field('building_area');
	} 
	wp_reset_postdata();
    }

    return $total;
 }


 //get all the sub category of a category

function lsh_output_sub_list_of_category( $term_id ){

    $loc_lists = '<div>'."\n";

    $term = get_term_by('term_id', $term_id, 'countries');

    $loc_lists .= '<h3>' . __(esc_attr($term->name), 'lsh'). '</h3>'."\n";

     $child_terms = get_terms('countries', array( 
	 
	 'child_of' => $term_id,
	 'orderby'  => 'term_id',
	 'order'    => 'ASC',
	) );

    if( $child_terms ){
     
	$loc_lists .= '<div class="vertical_slider">'."\n"; 
    
        foreach( $child_terms as $child_term ){
	
        $loc_lists .= '<div><a href="' .esc_url( get_category_link( $child_term->term_id ) ). '">' . __(esc_attr($child_term->name), 'lsh') . '</a></div>'."\n";
	}   
    
        $loc_lists .= '</div>';
    }

    $loc_lists .= '</div>'."\n";	

    return $loc_lists;
}

 //add featured image
add_theme_support( 'post-thumbnails' );


//ajax filter for premium projects 
//for users login or not log in

function lsh_premium_projects_ajax_handler(){

	global $sitepress;
	$sitepress->switch_lang($_POST['cu_lang']);
    $args['post_type'] = 'projects';
    $args['posts_per_page'] = -1;
    $args['post_status'] = 'publish';
    $args['suppress_filters'] = 0;

	if( !isset($_POST['types']) ){ $_POST['types'] = 'all';}
	if( !isset($_POST['country']) ){ $_POST['country'] = 'all';}

   if( trim($_POST['types']) == 'all' && trim($_POST['country']) == 'all'){
	$args['tax_query']['relation'] = 'OR'; 
   }else{
	$args['tax_query']['relation'] = 'AND'; 
   }
  
    foreach( $_POST as $key => $value){
    
	 if( 'types' === $key ){

		 $t_type['taxonomy'] = 'types';
		 $t_type['field'] = 'slug';
		 $value = trim($value);

		 if( $value == 'all'){
			 $terms = get_terms( 'types' ); 
			 $term_slugs = wp_list_pluck( $terms, 'slug' ); //get all slugs of types taxonomy
			 $t_type['terms'] = $term_slugs;

		 }else{
		 
			 $t_type['terms'] = $value;
		 }

		 $args['tax_query'][] = $t_type;

	 }elseif( 'country' === $key ){
	  
	      $c_country['taxonomy'] = 'countries';
	      $c_country['field'] = 'slug';
	      $value = trim($value);

	      if( $value == 'all'){
			 $terms = get_terms( 'countries' ); 
			 $term_slugs = wp_list_pluck( $terms, 'slug' ); //get all slugs of countries taxonomy
			 $c_country['terms'] = $term_slugs;

		 }else{
		 
			 $c_country['terms'] = $value;
		 }

	      $args['tax_query'][] = $c_country;
	 }

    }

   // var_dump($args);
    $query = new WP_Query( $args );

    if( $query->have_posts() ){
 
	$p_results = array();
	while( $query->have_posts() ){
	   
	    $query->the_post();

		$p_pid = get_the_ID(); 
		$ct = wp_get_post_terms($p_pid, 'countries', array("fields" => "names"));//country terms of this post belong to

		$ct_name = esc_html( $ct[0]);//taxonomy name
		$p_image = get_the_post_thumbnail_url($p_pid, 'projects_taxonomy_list_img'); 

		$p_title = mb_strimwidth(wp_strip_all_tags(get_the_title($p_pid)), 0, 45, '…'); // title
		$p_property = get_field('property', $p_pid);//property

		$p_results[] = '<div class="benz_center_block load-more-hidden"><div class="beijing-pc">' . $ct_name .'</div> <img src="' . $p_image . '"><div class="commercial-tabs-block"><div class="container"><div class="commercial-tabs"><div class="commercial-tabs-content"><div class="commercial-tab1 commerical_two active"><h3>' . $p_title  . '</h3><div class="projects project_one"></div><div class="high-end high_end_one"> <a href="' . get_permalink() .'">' . __('Property', 'lsh'). ': ' . $p_property .'</a><p class="Beijing">' . $ct_name . '</p></div></div></div></div></div></div></div>';

	}
    
    wp_reset_postdata();
	echo json_encode($p_results);
    }

die();
}
add_action('wp_ajax_projects', 'lsh_premium_projects_ajax_handler'); 
add_action('wp_ajax_nopriv_projects', 'lsh_premium_projects_ajax_handler');

//generate theme settings page for header and footer
if( function_exists('acf_add_options_page') ) {
		
	acf_add_options_page(array(
		'page_title'	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug'		=> 'theme-general-settings',
		'capability'	=> 'read',
		'redirect'		=> false
		));
}



//function for output all projects count and square of types taxonomy

function lsh_output_types_count_and_area(){

	$p_types = get_terms('types', array(
		'orderby' => 'term_id',
		'order'  => 'ASC'
	));

	$p_items = "\n";

	foreach( $p_types as $p_type){

		$p_items .= '<div class="property">
						<div class="property_icon"><span><img src="' . get_template_directory_uri() . '/lsh-img/' . $p_type->slug . '.png" alt="icon"></span></div>
						<!--property_icon-->
						<h4>' . sprintf( __('%s Property', 'lsh'), $p_type->name ) .'</h4>
						<ul>
							<li>' . __('Projects', 'lsh'). '<span>' . $p_type->count . '</span></li>
							<li>' . __('Area', 'lsh') .' <span> ' . lsh_total_area_of_a_cat( $p_type->term_id ) . 'M <sup>2</sup></span></li>
						</ul>
					</div>'."\n";

		}
   return $p_items;
}

add_filter( 'gform_disable_notification', 'disable_notification', 10, 4 );
function disable_notification( $is_disabled, $notification, $form, $entry ) {
	    return true;
}

//function for output header region

function lsh_header_output(){

  return '<header id="header" class="respons">
    <div class="container main-cont">
      <div class="clearfix">
		<div class="logo"> <a href="' . get_home_url() .'"> <img src="' .  get_template_directory_uri() .'/assets/images/logo.png" alt="logo" class="white-logo"> <img src="'. get_template_directory_uri() .'/assets/images/logo2.png" alt="logo" class="blue-logo"> </a> </div>
        <a class="toggle-block"> <span class="toggle-btn">&nbsp;</span> </a>
		' . lsh_header_menu() . '
      </div>
    </div>
  </header>';

}

//function for output copyright region

function lsh_copyright_output(){

  return '<div class="copir-wrap" style="background-color: #0f1018;">
          <div class="container cfix">
            <div class="copy_right_block">
              <ul>
                <li> ' . __('Copyright © 2016 Leishinghong', 'lsh') . '</li>
                <li><a href="' . get_home_url() . '"><img src="' . get_template_directory_uri() . '/assets/images/flow-asia.png" alt="logo" /></a>' . __(' &nbsp; Design and Development', 'lsh') . '</li>
              </ul>
            </div>
          </div>
        </div>';

}

// add filter to gform field container

add_filter( 'gform_field_container', 'lsh_contact_form_style', 10, 6 );

function lsh_contact_form_style( $field_container, $field, $form, $css_class, $style, $field_content ) {

	if($field->id == 1){
	 $field_container = '<div class="left">'. $field_container;
	}elseif($field->id == 5){
	$field_container = '</div><div class="right">' . $field_container . '</div>';  
	}

	return $field_container;	
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button', 'lsh_contact_form_submit', 10, 2 );
function lsh_contact_form_submit( $button, $form ) {

			$button = str_replace( "gform_button", "btn btn_one gform_button", $button );
			return $button;
}

// get list of category for news
 function lsh_get_categories( $content = false ) {

	 $categories = get_categories(array('type' => 'post','child_of' => 0,'orderby' => 'name','order' => 'ASC','hide_empty' => false));

	 if( $categories ){
		$html = '';
		  
		 if( $content ){

			foreach ( $categories as $cate ){
			 $html .= '<div class="' . esc_attr($cate->slug ) . '"></div>';
			 }
			}else{
			  $html = '<ul id="stn" class="tabs sub-tabs-news">' ."\n";
			  $html .= '<li class="tab-link current" data-tab="all" offset="2">' . __('ALL', 'lsh'). '</li>'. "\n";
			  foreach ( $categories as $nu => $cat ){
				$real = $nu + 1;
				$html .= '<li class="tab-link" data-tab="tab-' . $real .'" offset="2">' . __(esc_attr( $cat->name ), 'lsh') . '</li>'. "\n"; 
			  }
			  $html .= '</ul>'; 
		 }

		return $html;
	 }
 }


//function get all slugs of category for news and events

function lsh_news_all_slug(){

	$cats = get_categories(array('type' => 'post','child_of' => 0,'orderby' => 'name','order' => 'ASC','hide_empty' => false));
	 $slugs = array('all');

	 foreach( $cats as $nu => $cat ){
		 $slugs[] = 'tab-'. ($nu+1);
		}

	 return $slugs;
}


//ajax filter for news and events
//for users login or not log in

function lsh_news_ajax_handler(){
	
global $wpdb;
global $sitepress;
$sitepress->switch_lang($_POST['cu_lang']);

$cats = get_categories(array('type' => 'post','child_of' => 0,'orderby' => 'name','order' => 'ASC','hide_empty' => false));
	 $cats_array = array();
	 foreach( $cats as  $nu => $cat ){
		 $cats_array[$cat->slug] = $nu+1;
		}
	 
 if( $_POST['tab_id'] == 'all' ){
	$ori_slug = '';
 }else{
   $ori_slug_tmp = explode("-", $_POST['tab_id']);
   $ori_slug = array_search($ori_slug_tmp[1], $cats_array);
 }

$paged = isset($_POST['paged']) ? $_POST['paged'] : 1; //paged

$args = array(
      'category_name' => $ori_slug,
      'post_type'     => 'post',
      'suppress_filters' => 0,
	  'paged' => $paged,
	  'orderby' => 'date',
	  'order'  => 'DESC',
      'posts_per_page' => 1
      );
$cat_posts =  new WP_Query( $args );

if( $cat_posts->have_posts() ){
    
    $results = array();

    while( $cat_posts->have_posts() ){
    $cat_posts->the_post();
	$op_pid = get_the_ID(); 
    $cs = get_the_category($op_pid);

    $item_name = esc_html( $cs[0]->name );//category name
	$item_slug = esc_attr($cs[0]->slug); // category slug
    $image = get_the_post_thumbnail_url($op_pid, 'news_list_img_news_page'); // thumbnail
    $ct = mb_strimwidth(wp_strip_all_tags(get_the_title($op_pid)), 0, 45, '…'); // title
    $ce = mb_strimwidth(wp_strip_all_tags(get_the_excerpt($op_pid)), 0, 200, '…');// excerpt

	$results[] = '<div class="tab_parent" id="' . $item_slug .'" data-filter="tab-' . $cats_array[$item_slug] . '" style="background-image: url(' . $image . ');"><div class="container"><div class="tab_content"></div><div class="tab_content"><div class="awareness_parent"><div class="awareness"><h5>' . $ct .'</h5><div class="date-t-mob"><span>' . get_the_date('Y-m-d') . '</span><span>' . $item_name . '</span></div><p>' . $ce . '</p></div><div class="tab_dt"><p>' . get_the_date('Y'). ' ' . __(get_the_date('M'), 'lsh') .  '<span>' . get_the_date('d'). '.</span></p><p class="vertical_content">' . $item_name . '</p></div></div><div class="view_more"><a href="' . get_permalink(). '">' . __('View More', 'lsh') . '</a></div></div></div></div>';

    }
	wp_reset_postdata();
   }else{ $result = array('flag' => 'none');}
 echo json_encode($results);
die();

}
add_action('wp_ajax_news', 'lsh_news_ajax_handler'); 
add_action('wp_ajax_nopriv_news', 'lsh_news_ajax_handler');
