<?php get_header();
    // Start the loop.
   while ( have_posts() ) : the_post();
   $types = wp_get_post_terms($post->ID, 'types', array("fields" => "names"));
 ?>

<div id="wrapper">
  <div class="header_patent beijing premiumpage-fix">
	<?php echo lsh_header_output(); ?> 
    <!-- /header -->
    <div class="bg_slide clearfix" id="vid">
	<?php $bgs = get_field('picture_gallery'); if( $bgs ):?>
      <div class="bg_slide_part">
			<?php foreach( $bgs as $bg ): ?>
			<figure> <img src="<?php echo $bg['sizes']['home_header_slider_img']; ?>" alt="logo"></figure>
			<?php endforeach; ?>
      </div>
    <?php endif; ?>
      <div class="commercial-tabs-block">
        <div class="container">
		<div class="beijing"> <span class="ic"><img id="before-img" src="<?php echo get_field('header_icon')['url'];?>"><?php if( $types ){ echo implode(", ", $types);}?></span>
			<h3><?php the_title();?></h3>
            <ul>
			<li><?php _e('Building Area', 'lsh'); ?><span><?php the_field('building_area');?>㎡</span></li>
			<li><?php _e('Investment Cost', 'lsh'); ?><span><?php the_field('investment_cost');?></span></li>
			<li><?php _e('Project Status', 'lsh'); ?><span><?php the_field('project_status');?></span></li>
            </ul>
			<div class="overview"> <a href="#"><?php _e('More information', 'lsh');?></a> </div>
          </div>
          <!--beijing--> 
        </div>
        <!--container--> 
      </div>
      <!--commercial-tabs-block--> 
      <!--bg_slid_part--> 
    </div>
    <!--bg_slide-->
    
	<div class="pause_btn" data-fancybox data-type="iframe" data-src="<?php the_field('video');?>" >
	<img src="<?php bloginfo('template_url');?>/assets/images/34.png" alt="logo"></div>
  </div>
  <!--headerpatent-->
  <section id="group_main">
    <div class="information">
      <div class="container">
	  <?php the_field('introduction_text');?> 
        <div class="basic_infromation">
		<?php $bis = get_field('basic_information'); if( $bis ):?>
          <div class="specification">
            <ul>
              <li>
			  <h4><?php _e('Basic information', 'lsh');?></h4>
              </li>
			  <?php foreach( $bis as $bi ):?> 
			  <li><span><?php echo $bi['title'];?>:</span>
			  <p><?php echo $bi['content'];?></p>
              </li>
			  <?php endforeach; ?> 
            </ul>
          </div>
		  <?php endif;?>
          <!--specification-->

		<?php $sps = get_field('Specification'); if( $sps ):?>
          <div class="specification">
            <ul>
              <li>
				<h4><?php _e('Specification', 'lsh');?></h4>
              </li>
				 <?php foreach( $sps as $sp ):?> 
			  <li><span><?php echo $sp['spec_title'];?>:</span>
			  <p><?php echo $sp['spec_content'];?></p>
              </li>
			  <?php endforeach; ?> 
             
            </ul>
          </div>
		  <?php endif; ?>
          <!--specification-->
			<?php $brochure = get_field('brochure_download_link'); if( $brochure):?>
			<div class="download_btn"><a href="<?php echo $brochure['url'];?>" class="down_btn"><?php the_field('brochure_download_text');?></a></div>
		  <?php endif; ?>
          <!--download_btn--> 
          
        </div>
        <!--basic_information--> 
      </div>
      <!--container--> 
    </div>
    <!--information-->
	<div class="info-post-bg" style="background-image: url(<?php echo get_field('highlight_picture')['url'];?>);"></div>
	<?php $hhs = get_field('highlight'); if( $hhs ):?>
    <div class="basic_things">
      <div class="container">
        <div class="property_parent">
		<?php foreach( $hhs as $hh ):?>
		<div class="basic_property"> <img src="<?php echo $hh['highlight_icon']['url']; ?>" alt="img">
            <div class="basic_property_one">
			<h5><?php echo $hh['highlight_title']?></h5>
			<?php echo $hh['highlight_text'];?>
            </div>
          </div>
		  <?php endforeach; ?>
        </div>
        <!--property_parent--> 
        
      </div>
      <!--container--> 
    </div>
	<?php endif; ?>
    <!--basic_things--> 
  </section>
  <!--main ends here-->
  
  <section class="premium_projects cooperation">
    <div class="container">
      <div class="looking_for">
	  <h3><?php the_field('coop_title');?></h3>
		<p><?php the_field('coop_desc');?></p>
		<div class="welcome-to-lsh"> <a href="<?php the_field('coop_link'); ?>" class="btn"><span><?php _e('LEARN MORE', 'lsh'); ?></span></a> </div>
      </div>
      <!--looking_for--> 
    </div>
    <!--container-->
    <footer id="colophon" class="fp-auto-height">
      <div class="container cfix">
	  <?php echo lsh_footer_menu(); ?> 
        <!-- /footer_row --> 
        
      </div>
	  <?php echo lsh_copyright_output(); ?> 
    </footer>
    <!-- /footer --> 
  </section>
</div>
<!-- /wrapper --> 
<!--JS--> 
<?php endwhile;
get_footer('project');
?>
