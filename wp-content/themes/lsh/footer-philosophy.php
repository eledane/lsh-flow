<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */

?>
<!--
<script>
window.jQuery || document.write('<script src="<?php bloginfo('template_url');?>/assets/js/vendor/jquery-3.1.0.min.js"><\/script>')
</script> 
-->
<?php wp_footer(); ?>
<script type="text/javascript">
        /* ==========================================================
           Philosophy Script
        ========================================================== */       
        jQuery("#philosophy-carousel .str_string").click(function(){
            var currPhil = jQuery(this).attr('data-philosophy');
            jQuery('#philosophy-carousel .operating_philosophy').hide();
            jQuery('#philosophy-carousel .operating_philosophy[data-philosophytarget="' + currPhil + '"]').show();
        });     
		jQuery("#brands-items .its_brand .two").click(function(){
			jQuery('#brands-items .b_one').hide();
            jQuery('#brands-items .b_two').show();
			jQuery('#brands-items .b_four').hide();
			jQuery('#brands-items .b_five').hide();
			jQuery('#brands-items .b_six').hide();
			jQuery('#brands-items .b_three').hide();
        }); 
		jQuery("#brands-items .its_brand .three").click(function(){
			jQuery('#brands-items .b_one').hide();
            jQuery('#brands-items .b_two').hide();
			jQuery('#brands-items .b_three').show();
			jQuery('#brands-items .b_four').hide();
			jQuery('#brands-items .b_five').hide();
			jQuery('#brands-items .b_six').hide();
        }); 
		jQuery("#brands-items .its_brand .one").click(function(){
			jQuery('#brands-items .b_one').show();
            jQuery('#brands-items .b_two').hide();
			jQuery('#brands-items .b_three').hide();
			jQuery('#brands-items .b_four').hide();
			jQuery('#brands-items .b_five').hide();
			jQuery('#brands-items .b_six').hide();
        });
		jQuery("#brands-items .its_brand .four").click(function(){
			jQuery('#brands-items .b_four').show();
            jQuery('#brands-items .b_two').hide();
			jQuery('#brands-items .b_three').hide();
			jQuery('#brands-items .b_one').hide();
			jQuery('#brands-items .b_five').hide();
			jQuery('#brands-items .b_six').hide();
        });  
		jQuery("#brands-items .its_brand .five").click(function(){
			jQuery('#brands-items .b_four').hide();
            jQuery('#brands-items .b_two').hide();
			jQuery('#brands-items .b_three').hide();
			jQuery('#brands-items .b_one').hide();
			jQuery('#brands-items .b_five').show();
			jQuery('#brands-items .b_six').hide();
        });
		jQuery("#brands-items .its_brand .six").click(function(){
			jQuery('#brands-items .b_four').hide();
            jQuery('#brands-items .b_two').hide();
			jQuery('#brands-items .b_three').hide();
			jQuery('#brands-items .b_one').hide();
			jQuery('#brands-items .b_five').hide();
			jQuery('#brands-items .b_six').show();
        });
    </script>
<script>
$(".str_string").click(function(e) {
  e.preventDefault();
  $(".str_string").removeClass('active');
  $(this).addClass('active');
})
</script>
<script>

    
jQuery(document).ready(function(){

    jQuery('.its_brand').slick({
        infinite: true,
        slidesToShow: 3,
        centerMode: true,
        variableWidth: true
    });

});

jQuery(document).ready(function(){
    var scroll = jQuery(window).scrollTop();
    //console.log( scroll );
    if (scroll >= 20 ) {

        jQuery('#header').addClass('scrolled');
        jQuery("nav, .t-symbol").addClass("change_color");
        jQuery(".logo .blue-logo").addClass("blue-logo1");

    } else {
        jQuery('#header').removeClass('scrolled');
        jQuery("nav, .t-symbol").removeClass("change_color");
        jQuery(".logo .blue-logo").removeClass("blue-logo1");
    }
})

jQuery(window).scroll( function() {    
        
    var scroll = jQuery(window).scrollTop();
    //console.log( scroll );
    if (scroll >= 20 ) {

        jQuery('#header').addClass('scrolled');
        jQuery("nav, .t-symbol").addClass("change_color");
        jQuery(".logo .blue-logo").addClass("blue-logo1");

    } else {
        jQuery('#header').removeClass('scrolled');
        jQuery("nav, .t-symbol").removeClass("change_color");
        jQuery(".logo .blue-logo").removeClass("blue-logo1");
    }
});

</script>
<script>
jQuery('.toggle-block').click(function(event){
        event.preventDefault();
        jQuery('.navigation').toggle();
        jQuery('#header').toggleClass("active");
		jQuery('#header').removeClass("respons");
    });
     //jQuery("nav ul li > ul").parent().prepend('<span></span>');
	 //jQuery("nav ul li span").click(function() {
	  //jQuery(this).parent().find('ul').slideToggle();
	  //jQuery(this).parent().find('ul ul').hide();
	 //});
jQuery('.navigation span').click(function() {
   jQuery(this).siblings(".navigation ul").slideToggle();
   jQuery(this).parent().toggleClass('active');
});
	function checkslider(){
	    if(jQuery(window).width() <= 767 && !(jQuery('.latest-news').hasClass('slick-initialized'))){ 
	    	jQuery('.latest-news').slick({
	    		dots: true
	    	});
	    }else{
	    	if((jQuery('.latest-news').hasClass('slick-initialized'))){
	    		jQuery('.latest-news').slick('unslick');
	    	}
	    }
	}
</script>
</body>
</html>
