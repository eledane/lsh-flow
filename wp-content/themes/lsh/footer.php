<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */

?>
<!--
<script>
window.jQuery || document.write('<script src="<?php bloginfo('template_url');?>/assets/js/vendor/jquery-3.1.0.min.js"><\/script>')
</script> 
-->
<?php wp_footer(); ?>
</body>
</html>
