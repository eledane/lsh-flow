<?php
get_header();
    // Start the loop.
if( isset($wp_query->query['types']) ){
 $q_types = $wp_query->query['types'];
}else{
 $q_types = 'all';
}

if( isset($wp_query->query['countries']) ){
 $q_countries = $wp_query->query['countries'];
}else{
 $q_countries = 'all';
}

$cu_lang = get_bloginfo("language") == 'zh-CN' ? 'zh-hans':'en';
?>
<div id="wrapper">
  <div class="header_patent pri_pro">
	<?php echo lsh_header_output(); ?> 
    <!-- /header -->
    <div class="bg_slide clearfix">
      <div class="bg_slide_part">
	  <figure> <img src="<?php bloginfo('template_url');?>/assets/images/k13.png" alt="logo">
          <div class="bg_content">
		  <h1><?php _e('Premium Projects', 'lsh'); ?></h1>
          </div>
          <!--bg_content--> 
        </figure>
      </div>
      <!--bg_slid_part--> 
    </div>
    <!--bg_slide--> 
  </div>
  <!--headerpatent-->
  <section id="group_main" class="pri_pro">
    <section class="quality_concept">

      <div class="premium_qty_parent">
        <div class="container">

          <form class="custom-form cfix" action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="pp-filter">
            <div class="types_block types_block_one">

              <div class="types_top_block cfix">
                <div class="block-select"> <a class="main-item-types" tabindex="1" >
				<h4><?php _e('TYPES', 'lsh'); ?>:</h4>
                  </a>
                  <ul  id="stn" class="tabs sub-tabs-types">
                    <li>
                      <input type="radio" name="types" value="all" checked="checked" id="all-types">
					  <label for="all-types" class="custom-radio"><?php _e('All', 'lsh'); ?></label>
                    </li>
					<?php
					  $types = get_terms([
						'taxonomy' => 'types',
						 'hide_empty' => false,
						]);

					foreach( $types as $t_item ){
					?>
					<li>
					<input type="radio" name="types" value="<?php echo trim($t_item->slug); ?>" id="<?php echo trim($t_item->slug); ?>-types">
					<label for="<?php echo $t_item->slug; ?>-types" class="custom-radio"><?php echo esc_html($t_item->name); ?></label>
                    </li>
					<?php }?>
                  </ul>
                </div>
              </div>
              <!-- types_top_block -->
              
              <div class="types_top_block cfix pr_main">
                <div class="cfix">

                  <div class="block-select countr"> <a class="main-item-countr" href="javascript:void(0);" tabindex="1" >
				  <h4><?php _e('COUNTRIES', 'lsh'); ?>:</h4>
                    </a>
                    <ul id="stn" class="tabs-menu tabchenge-nav sub-tabs-countr">
                      <li class="current" data-href="#tab1">
                        <input type="radio" name="country" value="all" id="countires-all">
						<label for="countires-all" class="custom-radio"><?php _e('All', 'lsh'); ?></label>
                      </li>
						<?php
						  $countries = get_terms(array(
							'taxonomy' => 'countries',
							'hide_empty' => false,
							'parent'    => 0
						));

						foreach( $countries as $nu => $c_item ){
						?>
						<li data-href="#tab<?php echo $nu+2; ?>">
						<input type="radio" name="country" value="<?php echo trim($c_item->slug); ?>" id="countires-<?php echo trim($c_item->slug);?>">
						<label for="countires-<?php echo trim($c_item->slug); ?>" class="custom-radio"><?php echo esc_html($c_item->name);?></label>
						</li>
						<?php } ?>
                    </ul>
                  </div>

                  <div class="types_top_block types_top_block-single cities-block" id="tab1" style="display:block;">
                    <ul class="types_block_bottom">
						<?php
						  $countries_all = get_terms(array(
							'taxonomy' => 'countries',
							'hide_empty' => false,
							'childless' => true
						));

						foreach( $countries_all as $c_item_all ){
						?>
						<li>
						<input type="radio" name="country" value="<?php echo trim($c_item_all->slug);?>" id="countries-<?php echo trim($c_item_all->slug); ?>">
						<label for="cities-<?php echo trim($c_item_all->slug);?>" class="custom-radio"><?php echo esc_html($c_item_all->name); ?></label>
						</li>
						<?php } ?>
                    </ul>
                  </div>

                  <!-- types_top_block  china -->
                  <div class="types_top_block types_top_block-single cities-block" id="tab2" style="display:none;">
                    <ul class="types_block_bottom">
						<?php
						  $countries_china = get_terms(array(
							'taxonomy' => 'countries',
							'hide_empty' => false,
							'child_of' => 3
						));

						foreach( $countries_china as $c_item_china){
						?>
						<li>
						<input type="radio" name="country" value="<?php echo trim($c_item_china->slug);?>" id="countries-<?php echo trim($c_item_china->slug); ?>">
						<label for="cities-<?php echo trim($c_item_china->slug);?>" class="custom-radio"><?php echo esc_html($c_item_china->name); ?></label>
						</li>
						<?php } ?>
                    </ul>
                  </div>

                  <!-- types_top_block  overseas-->
                  <div class="types_top_block types_top_block-single cities-block" id="tab3" style="display:none;">
                    <ul class="types_block_bottom">
						<?php
						  $countries_overseas = get_terms(array(
							'taxonomy' => 'countries',
							'hide_empty' => false,
							'child_of' => 2
						));

						foreach( $countries_overseas as $c_item_overseas){
						?>
						<li>
						<input type="radio" name="country" value="<?php echo trim($c_item_overseas->slug);?>" id="countries-<?php echo trim($c_item_overseas->slug); ?>">
						<label for="cities-<?php echo trim($c_item_overseas->slug);?>" class="custom-radio"><?php echo esc_html($c_item_overseas->name); ?></label>
						</li>
						<?php } ?>
                    </ul>
                  </div>
                  <!-- types_top_block --> 
                  
                </div>
                <!-- types_top_block --> 
			  </div>

				<!-- mobile begin  for countries -->
              <div class="types_top_block cfix pr_m_ip">
                <div class="cfix">
                  <div class="block-select countr"> <a class="main-item-countr" tabindex="1" >
                    <h4>COUNTRIES:</h4>
                    </a>
                    <ul id="pr_mob" class="tabs-menu tabchenge-nav sub-tabs-countr" style="display:none;">
					<li id="pr_bord_one" class="one-c" data-href="#tab1"><a><?php _e('All', 'lsh'); ?></a> <i class="arrow-down"></i>
                        <ul class="types_block_bottom" id="tab11" style="display:none;">
                          
    					<?php
						  $countries_all_m = get_terms(array(
							'taxonomy' => 'countries',
							'hide_empty' => false,
							'childless' => true
						));

						foreach( $countries_all_m as $c_m_n => $c_item_all_m ){
							$c_m_all_class = $c_m_n == 0 ? 'cust-mob-top' : 'cust-mob';
						?>
						<li>
						<input class="custom-r" type="radio" name="country" value="<?php echo trim($c_item_all_m->slug); ?>" id="countries-<?php echo trim($c_item_all_m->slug); ?>">
						<label for="cities-<?php echo trim($c_item_all_m->slug); ?>" id="<?php echo $c_m_all_class; ?>" class="custom-radio"><?php echo esc_html($c_item_all_m->name); ?></label>
                          </li>
						<?php } ?>   
                          
                        </ul>
                      </li>
                      <li id="pr_bord_two" data-href="#tab2"><a>China</a> <i class="arrow-down"></i>
                        <ul class="types_block_bottom" id="tab22" style="display:none;">
						<?php
						  $countries_china_m = get_terms(array(
							'taxonomy' => 'countries',
							'hide_empty' => false,
							'child_of' => 3
						));

						foreach( $countries_china_m as $c_c_m_nu =>  $c_item_china_m){
							$c_m_china_class = $c_c_m_nu == 0 ? 'cust-mob-top' : 'cust-mob';
						?>
						<li>
						<input class="custom-r" type="radio" name="country" value="<?php echo trim($c_item_china_m->slug); ?>" id="countries-<?php echo trim($c_item_china_m->slug); ?>">
						<label for="cities-<?php echo trim($c_item_china_m->slug); ?>" id="<?php echo $c_m_china_class; ?>" class="custom-radio"><?php echo esc_html($c_item_china_m->name); ?></label>
                          </li>
						<?php } ?> 
                        </ul>
                      </li>
					  <li id="pr_bord_three" data-href="#tab3"><a><?php _e('Overseas', 'lsh'); ?></a> <i class="arrow-down"></i>
                        <ul class="types_block_bottom" id="tab33" style="display:none;">
						<?php
						  $countries_overseas_m = get_terms(array(
							'taxonomy' => 'countries',
							'hide_empty' => false,
							'child_of' => 2
						));
						foreach( $countries_overseas_m as $c_o_m_nu =>  $c_item_overseas_m){
							$c_m_overseas_class = $c_o_m_nu == 0 ? 'cust-mob-top' : 'cust-mob';
						?>
						<li>
						<input class="custom-r" type="radio" name="country" value="<?php echo trim($c_item_overseas_m->slug); ?>" id="countries-<?php echo trim($c_item_overseas_m->slug); ?>">
						<label for="cities-<?php echo trim($c_item_overseas_m->slug); ?>" id="<?php echo $c_m_overseas_class; ?>" class="custom-radio"><?php echo esc_html($c_item_overseas_m->name); ?></label>
                          </li>
						<?php } ?> 
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
                <!-- types_top_block --> 
              </div>
			<!--mobile end -->		

            </div>
            <!-- types_block -->
			<input type="hidden" name="action" value="projects">
			<input type="hidden" name="cu_lang" value="<?php echo $cu_lang; ?>">
          </form>

        </div>
        <!--container--> 
      </div>
      <!-- premium_qty_parent -->
      
      <div class="benz_block_main">

      </div>
      <a href="#" class="btn btn_one load-content-primeum-projects">Loading more</a> </section>

    <!--container-->
    <footer id="colophon" class="fp-auto-height">
      <div class="container cfix">
	  <?php echo lsh_footer_menu(); ?> 
        <!-- /footer_row --> 
      </div>
	  <?php echo lsh_copyright_output(); ?> 
    </footer>
    <!-- /footer --> 
  </section>
</div>
<!-- /wrapper --> 
<!--JS--> 

<script>

jQuery(".main-item-types").click( function(){

  jQuery(this).next(".sub-tabs-types").toggle("fast")

});

jQuery(".main-item-types").click( function(){

  jQuery(this).parents(".sub-tabs-types").hide("fast")

});

</script> 
<script>

jQuery(".main-item-countr").click( function(){

  jQuery(this).next(".sub-tabs-countr").toggle("fast")

});

jQuery(".main-item-countr").click( function(){

  jQuery(this).parents(".sub-tabs-countr").hide("fast")

});

jQuery(".types_block_bottom .custom-r").click( function(){

  jQuery(this).parents(".sub-tabs-countr").hide("fast")

});

jQuery("#pr_bord_one").click(function(){
            jQuery('#tab11').show();
			jQuery('#tab22').hide();
			jQuery('#tab33').hide();
        });
jQuery("#pr_bord_two").click(function(){
            jQuery('#tab22').show();
			jQuery('#tab11').hide();
			jQuery('#tab33').hide();
        });
jQuery("#pr_bord_three").click(function(){
            jQuery('#tab33').show();
			jQuery('#tab11').hide();
			jQuery('#tab22').hide();
        });


</script> 
<script>
jQuery(document).ready(function(){

	//trigger click of element from url parameter
	jQuery('#<?php echo $q_types; ?>-types, #tab1 #countries-<?php echo $q_countries; ?>').trigger('click');
	
	var data = {
			  types: "<?php echo $q_types;?>",
			  country: "<?php echo $q_countries; ?>",
			  action : "projects",
			  cu_lang: "<?php echo $cu_lang; ?>"
			};
		
			jQuery.ajax({

			url: "<?php echo site_url() . '/wp-admin/admin-ajax.php'; ?>",
		    data: data, // form serialize
		    type: "POST", // POST
			dataType: "json",

			success:function(data){
					jQuery.each(data, function(key, val) { 
					jQuery('.benz_block_main').append(val);
				});

					showMore();
			},

			error:function(xhr,status,error){
				jQuery('.benz_block_main .benz_center_block').remove();
				alert('Nothing found here , please try another one!');
			}

			});

	// load more button click envent
	 function showMore(){
       jQuery(".benz_block_main .load-more-hidden").slice(0,1).removeClass("load-more-hidden");
	 }

    jQuery('.load-content-primeum-projects').on('click', function(e){
      e.preventDefault();
		showMore();
    });

	//form serialize for ajax
	jQuery('#pp-filter').submit(function(){

		jQuery.ajax({
			url:jQuery(this).attr('action'),
		    data:jQuery(this).serialize(), // form serialize
		    type:jQuery(this).attr('method'), // POST
			dataType: "json",

			success:function(data){
					jQuery('.benz_block_main .benz_center_block').remove();
					jQuery.each(data, function(key, val) { 
					jQuery('.benz_block_main').append(val);
				});

					showMore();
			},

			error:function(xhr,status,error){
				jQuery('.benz_block_main .benz_center_block').remove();
				alert('Nothing found here , please try another one!');
			}

			});

		   return false;
	    });

		//when form elements changed, call submit event
	    jQuery('#pp-filter').change(function(){
		    jQuery(this).submit();
		 });


  jQuery('.tabchenge-nav li').on('click', function(){

		jQuery('.tabchenge-nav li').removeClass('current');
		jQuery(this).addClass('current');
		var curr = jQuery(this).attr('data-href');

		jQuery('.types_top_block-single').each(function(){
				if ( '#'+jQuery(this).attr('id') == curr ){
					jQuery(this).slideDown(300);
				} else {
					jQuery(this).slideUp(300);
				}

		})

	})

});


jQuery(document).ready(function(){
    var scroll = jQuery(window).scrollTop();
    //console.log( scroll );
    if (scroll >= 20 ) {

        jQuery('#header').addClass('scrolled');
        jQuery("nav, .t-symbol").addClass("change_color");
        jQuery(".logo .blue-logo").addClass("blue-logo1");

    } else {
        jQuery('#header').removeClass('scrolled');
        jQuery("nav, .t-symbol").removeClass("change_color");
        jQuery(".logo .blue-logo").removeClass("blue-logo1");
    }
})

jQuery(window).scroll( function() {    
        
    var scroll = jQuery(window).scrollTop();
    //console.log( scroll );
    if (scroll >= 20 ) {

        jQuery('#header').addClass('scrolled');
        jQuery("nav, .t-symbol").addClass("change_color");
        jQuery(".logo .blue-logo").addClass("blue-logo1");

    } else {
        jQuery('#header').removeClass('scrolled');
        jQuery("nav, .t-symbol").removeClass("change_color");
        jQuery(".logo .blue-logo").removeClass("blue-logo1");
    }
});
</script> 
<?php
//endwhile;
get_footer();
?>
