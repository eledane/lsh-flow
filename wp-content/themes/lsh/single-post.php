<?php get_header();
    // Start the loop.
   while ( have_posts() ) : the_post();
   $types = wp_get_post_terms($post->ID, 'category', array("fields" => "names"));
 ?>
<div id="wrapper">
  <div id="events_list" class="header_patent header_patent_one">
	<?php echo lsh_header_output(); ?> 
    <!-- /header -->
    <div class="bg_slide clearfix">
      <div class="bg_slide_part"> </div>
      <!--bg_slid_part--> 
    </div>
    <!--bg_slide--> 
  </div>
  <!--headerpatent-->
  <div class="quality_concept">
    <div class="premium_qty_parent">
      <div class="container">
        <div class="safety_block">
		<div class="safety_block_one"> <a href="<?php echo get_permalink(302); ?>" class="back">Back</a>
            <div class="text_block">
			<h2><?php the_title(); ?></h2>
			<a href="#" class="drill"><img src="<?php bloginfo('template_url');?>/assets/images/dd.png"></a>
              <ul>
			  <li class="company"><?php if($types){ echo implode(", ", $types);}?></li>
			  <li><?php the_date('Y-m-d'); ?></li>
              </ul>
            </div>
            <div class="image_text_block">
			<?php the_content(); ?> 
            </div>
            <div class="images_main">
<!--
              <div class="image_one"> <img src="_ui/images/c1.png"> </div>
              <div class="image_two"> <img src="_ui/images/c2.png"> </div>
              <div class="image_three"> <img src="_ui/images/c3.png"> </div>
-->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--container--> 
  </div>
  <!--container-->
  <footer id="colophon" class="fp-auto-height">
      <div class="container cfix">
	  <?php echo lsh_footer_menu(); ?> 
        <!-- /footer_row --> 
      </div>
	  <?php echo lsh_copyright_output(); ?> 
    </footer>
  <!-- /footer -->
  </section>
</div>
<?php
endwhile;
get_footer('news');
?>
