<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */

?>
<script>
window.jQuery || document.write('<script src="<?php bloginfo('template_url');?>/assets/js/vendor/jquery-3.1.0.min.js"><\/script>')
</script> 
<?php wp_footer(); ?>
<script>
$(document).ready(function(){
    var scroll = $(window).scrollTop();
    //console.log( scroll );
    if (scroll >= 20 ) {

        $('#header').addClass('scrolled');
        $("nav, .t-symbol").addClass("change_color");
        $(".logo .blue-logo").addClass("blue-logo1");

    } else {
        $('#header').removeClass('scrolled');
        $("nav, .t-symbol").removeClass("change_color");
        $(".logo .blue-logo").removeClass("blue-logo1");
    }
})

$(window).scroll( function() {    
        
    var scroll = $(window).scrollTop();
    //console.log( scroll );
    if (scroll >= 20 ) {

        $('#header').addClass('scrolled');
        $("nav, .t-symbol").addClass("change_color");
        $(".logo .blue-logo").addClass("blue-logo1");

    } else {
        $('#header').removeClass('scrolled');
        $("nav, .t-symbol").removeClass("change_color");
        $(".logo .blue-logo").removeClass("blue-logo1");
    }
});
</script>

<script>

  $(document).ready(function(){
    //var scroller=jQuery.browser.webkit ? "body": "html";

    $('.premiumpage-fix .overview a').on('click', function(e){
      e.preventDefault();
      var target = $('#group_main').offset().top;
      $('body').animate({scrollTop:target},500);
    });

    $('.premiumpage-fix .commercial-tabs-block').addClass('load');


  });

</script>
</body>
</html>
