<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */

?>
<!--
<script>
window.jQuery || document.write('<script src="<?php bloginfo('template_url');?>/assets/js/vendor/jquery-3.1.0.min.js"><\/script>')
</script> 
-->
<?php wp_footer(); ?>
<script>
  jQuery(document).ready(function(){
      var scroll = jQuery(window).scrollTop();
//      console.log( scroll );
      if (scroll >= 10 ) {

          jQuery('#header').addClass('scrolled');
          jQuery("nav, .t-symbol").addClass("change_color");
          jQuery(".logo .blue-logo").addClass("blue-logo1");

      } else {
          jQuery('#header').removeClass('scrolled');
          jQuery("nav, .t-symbol").removeClass("change_color");
          jQuery(".logo .blue-logo").removeClass("blue-logo1");
      }
  });

  jQuery(window).scroll( function() {    
        
    var scroll = jQuery(window).scrollTop();
    //console.log( scroll );
    if (scroll >= 10 ) {

        jQuery('#header').addClass('scrolled');
        jQuery("nav, .t-symbol").addClass("change_color");
        jQuery(".logo .blue-logo").addClass("blue-logo1");

    } else {
        jQuery('#header').removeClass('scrolled');
        jQuery("nav, .t-symbol").removeClass("change_color");
        jQuery(".logo .blue-logo").removeClass("blue-logo1");
    }
});

</script>
</body>
</html>
