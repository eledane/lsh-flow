<?php
/**
 * Template name: Philosophy 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */
$nu_to_words = array('', 'one', 'two', 'three', 'four', 'five', 'six');
get_header(); ?>

<?php
    // Start the loop.
   while ( have_posts() ) : the_post();
   ?>

<style type="text/css">
.its_brand_1 {
	display: block;
}
.its_brand_2, .its_brand_3 {
	display: none;
}

@media (max-width:767px) {
.its_brand_2, .its_brand_3 {
	display: block;
}
}
</style>
<div id="wrapper">
<div class="header_patent philosophy">
  <?php echo lsh_header_output(); ?> 
  <!-- /header -->
  <div class="bg_slide clearfix">
    <div class="bg_slide_part">
      <figure> 
		<img src="<?php the_post_thumbnail_url(array(1440, 599));?>" alt="logo">
          <div class="bg_content mob">
			<h1><?php the_title(); ?></h1>
			<?php the_field('introduction_text'); ?>
          </div>
          <!--bg_content--> 
        </figure>
    </div>
    <!--bg_slid_part--> 
  </div>
  <!--bg_slide--> 
</div>
<!--headerpatent-->
<section id="group_main" class="phil">
  <section class="quality_concept">
    <div class="premium_qty_parent">
      <div class="container">
        <div class="premium_quality">
          <div class="quality_concept">
		  <h3><?php the_field('premium_and_quality_title');?></h3>
		  <span><?php the_field('premium_and_quality_subtitle'); ?></span>
		 <p> <?php the_field('premium_and_quality_text');?></p>
          </div>
          <!--quality_concept-->
          <div class="property_parent">
            <div class="hong_property">
			<figure><img src="<?php echo get_field('premium_and_quality_picture')['url'];?>" alt="img"></figure>
            </div>
            <div class="hong_property">
			<h5><?php the_field('company_title');?></h5>
			<?php $lsh_introductions = get_field('lsh_introduction'); if($lsh_introductions):?>
              <ul>
			  <?php foreach( $lsh_introductions as $intr ):?> 
			  <li><?php echo strip_tags($intr['bullet_point']);?></li>
			  <?php endforeach; ?>
              </ul>
			<?php endif; ?>
            </div>
          </div>
          <!--property_parent--> 
        </div>
        <!--premium_quality--> 
      </div>
      <!--container--> 
    </div>
    <!--premium_qty_parent-->
	<div class="philoso_bg"><img src="<?php echo get_field('big_picture')['url'];?>" alt="img"></div>
    <!--philoso_bg-->
<?php 
$values = get_field('values');
if($values):
?>
<section class="brand_value" id="brands-items" style="background-image: url(<?php echo get_field('value_background_picture')['url']; ?>)">
	<div class="container"> <span class="brand_val"><?php the_field('brand_value_title'); ?></span>
        <div class="brand_slide">
       <ul class="its_brand its_brand_1">
          <?php foreach( $values as $num => $value): 
			$loop_num = $num + 1;
			$loop_class = $loop_num == 1 ? '' : 'mobile';
			?>
				<li class="<?php echo $nu_to_words[$loop_num]; ?>">
				<div class="brand_items <?php echo $loop_class; ?>">
                <div class="brands">
				<div class="con"> <img src="<?php echo $value['value_icon']['url'];?>" alt=""> </div>
                <h4> <?php echo $value['value_title'];?></h4>
                </div>
                <!--brands--> 
              </div>
              <!--brand_items--> 
            </li>
			<?php endforeach; ?>
          </ul>
        </div>

		<?php foreach( $values as $c_nu => $va_con):
		$con_nu = $c_nu + 1;
		$brand_or = $con_nu%3 == 0 ? 3 : $con_nu%3;
		$display_val = $con_nu == 1 ? 'block' : 'none';
		?>
        <!--brand_slide-->
		<div class="brand_content b_<?php echo $nu_to_words[$con_nu];?>" data-brandtarget="brand-<?php echo $nu_to_words[$brand_or];?>" style="display:<?php echo $display_val; ?>;">
		<p><?php echo $va_con['value_text']; ?></p>
        </div>
     <?php endforeach; ?>
      </div>

      <!--container-->
      </div>
      <!--container--> 
    </section>
<?php endif; ?>
  </section>
  <!--quality_concept-->

  <?php $ops = get_field('philosophy_items'); if( $ops ): ?>
  <section class="group_structure philosophy" id="philosophy-carousel" style="background-image: url(<?php echo get_field('operating_philosophy_background_picture')['url']; ?>)">
    <div class="container">
      <section class="structure">
	  <h3><?php the_field('operating_philosophy_title'); ?></h3>

		<?php foreach( $ops as $o_nu => $op ):
			$op_class = $o_nu == 0 ? 'active' : '';
		?>
		<div class="str_string <?php echo $op_class; ?>" data-philosophy="philosophy-<?php echo $nu_to_words[$o_nu+1];?>">
          <div class="str_img"> <span><?php echo $op['philosophy_title']; ?></span> </div>
        </div>
        <?php endforeach; ?> 
        <!--str_string--> 
      </section>
      <!--structure-->
	
	<?php foreach( $ops as $con_nu => $con_op ):?>
	<div class="operating_philosophy clearfix" data-philosophytarget="philosophy-<?php echo $nu_to_words[$con_nu+1];?>">
        <div class="operating_content">
		<figure> <img src="<?php echo $con_op['philosophy_picture']['url']; ?>" alt="img"> </figure>
        </div>
        <!--operating_content-->
        <div class="operating_content">
		<?php echo $con_op['philosophy_content']; ?>
        </div>
        <!--operating_content--> 
      </div>
	  <?php endforeach; ?>
      
      <!--operating_philosophy--> 
    </div>
    <!--container--> 
  </section>
  <?php endif; ?>
  <!--structure-->
  
	  <?php $culture = get_field('culture_item'); if( $culture ): $flag = 0 ?>
  <div class="culture_spirit_parent">

	<?php foreach( $culture as  $cul_nu => $cul_item):
	?>
    <section class="culture_spirit">
      <div class="container">
		<?php if ( $cul_nu == 0):?>
		<h3><?php _e('Culure &amp; Spirit', 'lsh'); ?> </h3>
		<?php endif; ?>
        <div class="culture_parent">
          <div class="culture_string">
            <div class="culture">
              <div class="spirit">
                <div class="cp_value">
                  <div class="value_in_shape">
				  <div class="shape_with_logo"> <img src="<?php echo $cul_item['culture_icon']['url']; ?>" alt="logo"> <span><?php echo $cul_item['culture_title']; ?></span> </div>
                  </div>
                  <!--value_inshpe--> 
                </div>
                <!--cp_value--> 
              </div>
              <!--spirit-->
			<?php //var_dump($cul_item['culture_left']);
			if( $cul_item['culture_left'][0]['acf_fc_layout'] == 'text_content'): ?>
              <div class="spirit">
				<?php echo $cul_item['culture_left'][0]['culture_text_content']; ?>
              </div>
			<?php elseif($cul_item['culture_left'][0]['acf_fc_layout'] == 'image_content'): $flag+=1; ?>
				<div class="spirit">
					<figure class="clipping_<?php echo $nu_to_words[$flag];?>" style="background-image: url(<?php echo $cul_item['culture_left'][0]['culture_image_content']['url'];?>)"></figure>
	            </div>	
			<?php endif; ?>	
              <!--spirit--> 
            </div>
            <!--culture-->
            <div class="culture">
			<?php echo $cul_item['culture_right']; ?> 
            </div>
            <!--culture--> 
          </div>
        </div>
        <!--culture_parent--> 
      </div>
      <!--container--> 
    </section>
	<?php endforeach; ?>
    <!--culture_spirit--> 

  </div>
  <?php endif; ?>
  <!--culture_spirit_parent-->
  <section class="learn_more p_phy">
    <div class="container">
	<h3><?php the_field('title', 'option');?></h3>
			<?php the_field('text', 'option'); ?> 
    </div>
    <!--container--> 
  </section>
  <!--learn_more--> 
  
</section>
<!--main ends here-->
<section class="premium_projects phil">
  <div class="container">
    <div class="premium">
                <!--property-->
				<?php echo lsh_output_types_count_and_area(); ?> 
                <!--property--> 
            </div>
    <!--premium--> 
  </div>
  <!--container-->
  <footer id="colophon" class="fp-auto-height">
      <div class="container cfix">
	  <?php echo lsh_footer_menu(); ?> 
        <!-- /footer_row --> 
        
      </div>
	  <?php echo lsh_copyright_output(); ?> 
    </footer>
  <!-- /footer --> 
</section>
</div>
<!-- /wrapper --> 
<!--JS--> 
<?php
endwhile;
get_footer('philosophy');
?>
