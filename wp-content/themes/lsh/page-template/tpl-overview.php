<?php
/**
 * Template name: Overview 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
    // Start the loop.
   while ( have_posts() ) : the_post();
   ?>

<div id="wrapper">
  <div class="header_patent overveiw-fix-2">
  <?php echo lsh_header_output(); ?> 
    <!-- /header -->
    <div class="bg_slide clearfix">
      <div class="bg_slide_part">
		<figure> 
		<img src="<?php the_post_thumbnail_url(array(1440, 599));?>" alt="logo">
          <div class="bg_content mob">
			<h1><?php the_title(); ?></h1>
			<?php the_field('short_introduction'); ?>
          </div>
          <!--bg_content--> 
        </figure>
      </div>
      <!--bg_slid_part--> 
    </div>
    <!--bg_slide--> 
  </div>
  <!--headerpatent-->
  
  <section id="group_main">
  <div class="company_overview" style="background-image: url(<?php echo get_field('coverage_picture')['url'];?>)">
      <div class="container">
        <div class="who_we_are">
          <h3><?php _e('Who We Are', 'lsh'); ?></h3>
          <div class="for_md">
                        <div class="we_are">
						<?php the_field('who_we_are_left'); ?>
                        </div>
                        <!--we_are-->
                        
                        <div class="we_are">
						<?php the_field('who_we_are_right'); ?>
                        </div>
                        <!--we_are--> 
                    </div>
          <!--for_md-->
          <div class="for_xs">
                        <div class="we_are">
						<?php the_field('who_we_are_left'); ?>
						<?php the_field('who_we_are_right'); ?>
                        </div>
                        <!--we_are--> 
                    </div>
          <!--for_xs--> 
        </div>
        
        <!--who_we_are-->
        <div class="business_coverage">
				<h3><?php the_field('coverage_title');?></h3>
				<span><?php the_field('coverage_subtitle'); ?></span>
				<?php the_field('coverage_text'); ?>
                </div>
        <!--business_coverage--> 
        
      </div>
      
      <!--container--> 
      
    </div>
    
    <!--company_overview-->
    <section class="learn_more">
    <div class="container">
		<h3><?php the_field('title', 'option');?></h3>
			<?php the_field('text', 'option'); ?> 
	    </div>
            <!--container--> 
        </section>
    <!--learn_more--> 
    
  </section>
  
  <!--main ends here-->
  
  <section class="premium_projects pre_mobile">
    <div class="container">
      <div class="premium">
                <!--property-->
				<?php echo lsh_output_types_count_and_area(); ?> 
                <!--property--> 
            </div>
      <!--premium--> 
    </div>
    
    <!--container-->
    <footer id="colophon" class="fp-auto-height">
      <div class="container cfix">
	  <?php echo lsh_footer_menu(); ?> 
        <!-- /footer_row --> 
        
      </div>
	  <?php echo lsh_copyright_output(); ?> 
    </footer>
    <!-- /footer --> 
    
  </section>
</div>

<!-- /wrapper --> 

  <?php endwhile; ?>
<?php get_footer('overview');?>
