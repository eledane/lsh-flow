<?php
/**
 * Template name: Contact 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
    // Start the loop.
   while ( have_posts() ) : the_post();
   ?>

<div id="wrapper">
  <div class="header_patent">
  <?php echo lsh_header_output(); ?> 
    <!-- /header -->
    
    <div class="bg_slide clearfix">
      <div class="bg_slide_part">
	  <figure> <img src="<?php the_post_thumbnail_url(array(1440, 599));?>" alt="logo">
          <div class="bg_content">
		  <h1><?php the_title(); ?></h1>
		  <?php the_content(); ?> 
          </div>
          <!--bg_content--> 
        </figure>
      </div>
      <!--bg_slid_part--> 
    </div>
    <!--bg_slide--> 
  </div>
  
  <!--headerpatent-->
  <?php $offices = get_field('offices');?>  
  <section id="group_main">
    <section class="quality_concept">
      <div class="premium_qty_parent">

       <?php if( $offices ):?>
		<div class="information_right information_right_two active"> 
		<img class="map1" src="<?php echo $offices[0]['map']['url'];?>">
		<img class="map2" src="<?php echo $offices[1]['map']['url'];?>">
          <div class="image_block"> </div>
        </div>

        <div class="container">

          <div class="information_block">

            <div class="information_block_main">
			  <div class="information_right"><a href="<?php echo $offices[0]['map_link']?>"> <img src="<?php echo $offices[0]['map']['url'];?>"></a>
                <div class="image_block"> </div>
              </div>
              <div id="info-cont" class="information_left">
			  <h2><?php echo $offices[0]['office_name']; ?><small><?php echo $offices[0]['office_legal_name']; ?></small></h2>
                <ul>
					<?php if( !empty( $offices[0]['phone_number'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone.png"><?php echo $offices[0]['phone_number']; ?></li>
					<?php endif; ?>
					<?php if( !empty( $offices[0]['fax_number'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone1.png"><?php echo $offices[0]['fax_number']; ?></li>
					<?php endif; ?>
					<?php if( !empty( $offices[0]['email_address'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone2.png"><?php echo $offices[0]['email_address']; ?></li>
				  <?php endif; ?>
					<?php if( !empty( $offices[0]['address'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone3.png"><?php echo $offices[0]['address']; ?></li>
					<?php endif; ?>
                </ul>
              </div>
            </div>

            <div class="information_block_main information_block_main_one">
              <div class="information_right information_right_one"><a href="<?php echo $offices[1]['map_link']?>"> <img src="<?php echo $offices[1]['map']['url'];?>"></a>
                <div class="image_block"> </div>
              </div>
              <div id="info-cont" class="information_left">
            	<h2><?php echo $offices[1]['office_name']; ?><small><?php echo $offices[1]['office_legal_name']; ?></small></h2>
                <ul>
					<?php if( !empty( $offices[1]['phone_number'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone.png"><?php echo $offices[1]['phone_number']; ?></li>
					<?php endif; ?>
					<?php if( !empty( $offices[1]['fax_number'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone1.png"><?php echo $offices[1]['fax_number']; ?></li>
					<?php endif; ?>
					<?php if( !empty( $offices[1]['email_address'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone2.png"><?php echo $offices[1]['email_address']; ?></li>
				  <?php endif; ?>
					<?php if( !empty( $offices[1]['address'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone3.png"><?php echo $offices[1]['address']; ?></li>
					<?php endif; ?>
                </ul>    
              </div>
            </div>

            <div class="mobile_address">
              <ul class="tabs">
			  <li id="beij" class="tab-link current" data-tab="tab-1"><?php _e('Beijing', 'lsh'); ?></li>
			  <li id="shan" class="tab-link" data-tab="tab-2"><?php _e('shanghai', 'lsh'); ?></li>
              </ul>
              <div id="tab-1" class="information_left current">
				<h2><?php echo $offices[0]['office_name']; ?><small><?php echo $offices[0]['office_legal_name']; ?></small></h2>
				<ul>
					<?php if( !empty( $offices[0]['phone_number'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone.png"><?php echo $offices[0]['phone_number']; ?></li>
					<?php endif; ?>
					<?php if( !empty( $offices[0]['fax_number'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone1.png"><?php echo $offices[0]['fax_number']; ?></li>
					<?php endif; ?>
					<?php if( !empty( $offices[0]['email_address'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone2.png"><?php echo $offices[0]['email_address']; ?></li>
				  <?php endif; ?>
					<?php if( !empty( $offices[0]['address'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone3.png"><?php echo $offices[0]['address']; ?></li>
					<?php endif; ?>
                </ul>
              </div>
              <div id="tab-2" class="information_left">
				<h2><?php echo $offices[1]['office_name']; ?><small><?php echo $offices[1]['office_legal_name']; ?></small></h2>
                <ul>
					<?php if( !empty( $offices[1]['phone_number'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone.png"><?php echo $offices[1]['phone_number']; ?></li>
					<?php endif; ?>
					<?php if( !empty( $offices[1]['fax_number'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone1.png"><?php echo $offices[1]['fax_number']; ?></li>
					<?php endif; ?>
					<?php if( !empty( $offices[1]['email_address'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone2.png"><?php echo $offices[1]['email_address']; ?></li>
				  <?php endif; ?>
					<?php if( !empty( $offices[1]['address'])):?>
				  <li><img id="cont-img" src="<?php bloginfo('template_url');?>/assets/images/phone3.png"><?php echo $offices[1]['address']; ?></li>
					<?php endif; ?>
                </ul> 
              </div>
            </div>
          </div>
        </div>
		<?php endif; ?>

        <div class="share_blcok">
          <div class="container">
            <div class="form_block">
			<h3><?php the_field('contact_form_title'); ?></h3>
              <p class="feedback"><?php echo strip_tags(get_field('contact_form_description'));?></p>
			  <?php echo do_shortcode(get_field('contact_form'));?> 
            </div>
          </div>
        </div>
      </div>
      
      <!--container--> 
      
    </section>
    
    <!--container-->
    
    <footer id="colophon" class="fp-auto-height">
      <div class="container cfix">
	  <?php echo lsh_footer_menu(); ?> 
        <!-- /footer_row --> 
        
      </div>
	  <?php echo lsh_copyright_output(); ?> 
    </footer>
    
    <!-- /footer --> 
    
  </section>
</div>

<!-- /wrapper --> 

  <?php endwhile; ?>
<?php get_footer('contact');?>
