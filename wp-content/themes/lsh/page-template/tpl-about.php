<?php
/**
 * Template name: About 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
    // Start the loop.
   while ( have_posts() ) : the_post();
   ?>

<style type="text/css">
  #china::before {
    background: none;
  } 
  #china:hover::before {
    background: rgba(18, 106, 183, 0.7);
  } 
  #overseas::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: none;
    z-index: -1;    
  } 
  #overseas:hover::before {
    background: rgba(18, 106, 183, 0.7);
  }
  .lsh-plaza-block {
      z-index: 3;
  }
  .outre_circle {
    overflow: hidden;
  } 
  .outre_circle:after {
      content: '';
      position: absolute;
      top: 0px;
	  background: url(<?php bloginfo('template_url');?>/assets/images/k1.png) no-repeat center center;
      width: 100%;
      height: 100%;
      background-size: cover;
      z-index: -1;
  }
  .fs-lg.banner-img:nth-child(2):after, .fs-lg.banner-img:nth-child(3):before {
      background: none;
  }  
</style>
<div id="wrapper">
  <?php echo lsh_header_output(); ?> 
<!-- /header -->

<section id="main">
<section class="section sec1 about-add-wrap" id="section6" style="background-image: url(<?php the_post_thumbnail_url( array(1475, 983));?>)">
    <div class="container">
	<?php $items = get_field('about_items'); if( $items ):?>
      <div class="content-buble">
		<?php 
		foreach( $items as $item ):
			$i_title = '';
			$i_text = '';	
		?>
		<?php
		foreach($item['about_item'] as $nu => $item_sub ){
				if( $nu == 0 ){
					$i_title = '<div class="named"><a href="' . $item_sub['about_sub_link'] .'">' . $item_sub['about_sub_text'] . '</a></div>';
				}else{
					$i_text .= '<li><a href="' . $item_sub['about_sub_link'] . '">' . $item_sub['about_sub_text']. '</a></li>';
				}
			
			}
		?>
        <div class="buble">
			<?php echo $i_title; ?> 
		<?php if(!empty($i_text)):?>
          <ul>
			  <?php echo $i_text; ?> 
		  </ul>
		<?php endif; ?>
        </div>
		<?php endforeach; ?>

      </div>
	  <?php endif; ?>
    </div>
  </section>
  <!-- /footer -->
  </div>
</section>
<!-- /main -->

</div>
<!-- /container --> 
<?php
endwhile;
get_footer();
?>
