<?php
/**
 * Template name: News 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */
$cu_lang = get_bloginfo("language") == 'zh-CN' ? 'zh-hans':'en';
get_header(); ?>

<?php
    // Start the loop.
   while ( have_posts() ) : the_post();
   ?>

<div id="wrapper">
  <div class="header_patent events">
	<?php echo lsh_header_output(); ?> 
    <!-- /header -->
    
    <div class="bg_slide clearfix">
      <div class="bg_slide_part">
        <figure> <img src="<?php the_post_thumbnail_url(array(1440, 599));?>" alt="logo">
          <div class="bg_content">
		  <h1><?php the_title();?></h1>
		  <?php the_content(); ?>
          </div>
          <!--bg_content--> 
        </figure>
      </div>
      <!--bg_slid_part--> 
    </div>
    <!--bg_slide--> 
  </div>
  <!--headerpatent-->
  <section id="group_main">
    <div class="tab_section">
      <div class="block-select"> <a class="main-item-news" tabindex="1" >News</a>
	  <?php echo lsh_get_categories(); ?> 
      </div>

      <div id="tab-1" class="tab-content current" style="height: auto;">
        <div class="all">
		<?php echo lsh_get_categories(true);?>
        </div>
        <!--all--> 
        
      </div>
    </div>
    <!--tab_section-->
    
    <div class="load_more"> <a class="btn" href="#"> <span>Loading more</span> </a> </div>
  </section>
  <!--main ends here-->
  
  <section class="n_and_ev">
    <footer id="colophon" class="fp-auto-height">
      <div class="container cfix">
	  <?php echo lsh_footer_menu(); ?> 
        <!-- /footer_row --> 
        
      </div>
	  <?php echo lsh_copyright_output(); ?> 
    </footer>
    <!-- /footer --> 
    
  </section>
  <!--n_and_ev--> 
  
</div>
<!-- /wrapper --> 

<script>   
  jQuery(document).ready(function(){

	// document ready event

function do_ajax(){

	var js_slugs = <?php echo json_encode( lsh_news_all_slug());?>;

if( js_slugs !== null && js_slugs.length !== 0 ){
	for( var i = 1; i < js_slugs.length; i++){

		var data = { 
				    action: 'news', 
					cu_lang : "<?php echo $cu_lang; ?>",
					tab_id: js_slugs[i]
				};

			jQuery.ajax({
				url: "<?php echo site_url() . '/wp-admin/admin-ajax.php'; ?>",
				type: "POST",
				dataType: "json",
				data: data,
				success:function (data){
					if( jQuery.trim(data)){
					jQuery.each(data, function(key, val) { 

					var app_class = jQuery(val).filter('.tab_parent').attr('id');
					jQuery('.all .'+ app_class).append(val);
					});
					}
				},
				error:function(xhr,status,error){
					alert(xhr.status);
				}
			});
    	}
 }

}

do_ajax();


	// tab click to switch
	jQuery('ul.tabs li').click(function(){

		var tab_id = jQuery(this).attr('data-tab');

        if ( tab_id != 'all'){
            jQuery('#tab-1 .tab_parent').each(function(){
                if( jQuery(this).attr('data-filter') == tab_id ){
                    jQuery(this).slideDown(300);
                } else {
                    jQuery(this).slideUp(300);
                }
            });
        } else {
            jQuery('#tab-1 .tab_parent').slideDown(300);
        }

        jQuery('ul.tabs li').removeClass('current');
		jQuery(this).addClass('current');

	});


	
	// load more button click event
    jQuery('.load_more a').on('click', function(e){
        e.preventDefault();
	
		var tab_id = jQuery('#stn .current').attr('data-tab');
        var offset = Number(jQuery('#stn .current').attr('offset'));

		var data = { 
				    action: 'news', 
					cu_lang : "<?php echo $cu_lang; ?>",
					tab_id: tab_id,
					paged: offset
				};

			jQuery.ajax({
				url: "<?php echo site_url() . '/wp-admin/admin-ajax.php'; ?>",
				type: "POST",
				dataType: "json",
				data: data,
				success:function (data){
					if( jQuery.trim(data)){
					jQuery.each(data, function(key, val) { 

					var app_class = jQuery(val).filter('.tab_parent').attr('id');
					jQuery('.all .'+ app_class).append(val);
					});

					var next_page = offset + 1;
					jQuery('#stn .current').attr('offset', next_page);
					}
				},
				error:function(xhr,status,error){
					alert('internet error, please try later!');
				}
			});

  /*      
	  jQuery.ajax({
            url : 'lsh_news&events.json',
            data: {
                'ofset' : ofset,
                'count' : 10
                },
            method:'POST',
            success : function(data){
                if ( data.fullSteack > ofset +  data.count ){

                } else {
                    jQuery('.load_more a').css('display', 'none');
                }
                jQuery('#tab-1 .all').append( data.content );

            }
        });
   */

    }); 

});


jQuery(document).ready(function(){
    var scroll = jQuery(window).scrollTop();
    //console.log( scroll );
    if (scroll >= 20 ) {

        jQuery('#header').addClass('scrolled');
        jQuery("nav, .t-symbol").addClass("change_color");
        jQuery(".logo .blue-logo").addClass("blue-logo1");

    } else {
        jQuery('#header').removeClass('scrolled');
        jQuery("nav, .t-symbol").removeClass("change_color");
        jQuery(".logo .blue-logo").removeClass("blue-logo1");
    }
})

jQuery(window).scroll( function() {    
        
    var scroll = jQuery(window).scrollTop();
    //console.log( scroll );
    if (scroll >= 20 ) {

        jQuery('#header').addClass('scrolled');
        jQuery("nav, .t-symbol").addClass("change_color");
        jQuery(".logo .blue-logo").addClass("blue-logo1");

    } else {
        jQuery('#header').removeClass('scrolled');
        jQuery("nav, .t-symbol").removeClass("change_color");
        jQuery(".logo .blue-logo").removeClass("blue-logo1");
    }
});

   </script>
<?php
endwhile;
get_footer('home');
?>
