<?php
/**
 * Template name: LSH-group 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
    // Start the loop.
   while ( have_posts() ) : the_post();
   ?>

<div id="wrapper">
  <div class="header_patent">
	<?php echo lsh_header_output(); ?> 
    <!-- /header -->
    
    <div class="bg_slide clearfix">
      <div class="bg_slide_part">
        <figure> 
		<img src="<?php the_post_thumbnail_url(array(1440, 599));?>" alt="logo">
          <div class="bg_content mob">
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
          </div>
          <!--bg_content--> 
        </figure>
      </div>
      <!--bg_slid_part--> 
    </div>
    <!--bg_slide--> 
  </div>
  <!--headerpatent-->
  
  <section id="group_main">
    <div class="lsh_group">
      <div class="container">
        <div class="lsh_group_content">
		<h3><?php the_field('group_title');?></h3>
          <div class="lsh_content">
			<?php the_field('group_introduction_text_left');?>
          </div>
          <!--lsh_content-->
          
          <div class="lsh_content">
		  <?php the_field('group_introduction_text_right');?> 
          </div>
          <!--lsh_content--> 
          
        </div>
        <!--lis_grou_content--> 
        
      </div>
      <!--container--> 
      
    </div>
    <!--lhs_group-->
    
    <div class="group_structure">
      <div class="container">
        <section class="structure">
		<h3><?php the_field('structure_title'); ?></h3>
		<?php $departments = get_field('departments'); if( $departments ):?>
          <ul class="tabs">
			<?php foreach( $departments as $nu => $de ): 
			$se = $nu + 1;
			$se_class = $se == 1 ? 'current' : ''; 
			?>
			<li class="tab-link <?php echo $se_class; ?>" data-tab="tab-<?php echo $se; ?>">
              <div class="str_string">
				<div class="str_img"> 
					<span> 
					<img  class="img_hover" src="<?php echo $de['department_active_icon']['url']; ?>" alt="logo"> 
					<img  class="img_default" src="<?php echo $de['department_icon']['url'];?>" alt="logo">						
				</span> 
				</div>
				<span class="str_name"><?php echo $de['department_title']; ?></span> </div>
            </li>
			<?php endforeach; ?> 
          </ul>
		<?php endif; ?>

		<?php if( $departments): ?>
			<?php foreach( $departments as $nu_con => $de_con): 
			$se_con = $nu_con +	1;
			$se_con_class = $se_con == 1 ? 'current' : '';
			?>
				<div id="tab-<?php echo $se_con; ?>" class="tab-content <?php echo $se_con_class; ?>">
				<p><?php echo $de_con['department_text'];?></p> 
			  </div>
			<?php endforeach; ?> 
			<?php endif; ?>
			<div class="welcome-to-lsh"> <a href="<?php the_field('lsh_group_site_button_link');?>" class="btn"><span><?php the_field('lsh_group_site_button_title'); ?></span></a> </div>
        </section>
        <!--structure--> 
        
      </div>
      <!--container--> 
      
    </div>
    <!--structure-->
    
    <div class="learn_more">
	<div class="container">
		<h3><?php the_field('title', 'option');?></h3>
			<?php the_field('text', 'option'); ?> 
		</div>
      <!--container--> 
      
    </div>
    <!--learn_more--> 
    
  </section>
  <!--main ends here-->
  
  <section class="premium_projects">
    <div class="container">
      <div class="premium">  
        <!--property--> 
		<?php echo lsh_output_types_count_and_area(); ?> 
        <!--property--> 
      </div>
      <!--premium--> 
    </div>

    <!--container-->
    <footer id="colophon" class="fp-auto-height">
      <div class="container cfix">
	  <?php echo lsh_footer_menu(); ?> 
        <!-- /footer_row --> 
        
      </div>
	  <?php echo lsh_copyright_output(); ?> 
    </footer>
    <!-- /footer --> 
    
  </section>
</div>

<!-- /wrapper --> 

	<?php endwhile; ?>
<?php get_footer('group'); ?>
