<?php
/**
 * Template name: Landing 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
    // Start the loop.
   while ( have_posts() ) : the_post();
   ?>

<div id="wrapper">
  <div class="header_patent header_patent_two two_v_circles">
  <?php echo lsh_header_output(); ?> 
    <!-- /header -->
    
    <div class="bg_slide bg_slide_one clearfix">
      <div class="primeum-v2-page">
        <figure> <img src="<?php the_post_thumbnail_url(array(1440, 599));?>" alt="logo">
          <div class="container">
            <div class="product_block">
              <section class="section sec1 china-overseas-wrap-one" id="section5">
                <div class="clearfix two_circles">
                  <div class="china-block china-block-one" id="china-one">
                    <div class="china_alignment">
							<?php 
							$china = 3;
							echo lsh_output_sub_list_of_category( $china);
							?>

                    </div>
                  </div>
                  <div class="china-block china-block-one" id="overseas-one">
                    <div class="china_alignment">
							<?php 
							$overseas = 2;
							echo lsh_output_sub_list_of_category( $overseas );
							?>

                    </div>
                  </div>
                  
                  <!--mob_btn--> 
                  
                </div>
              </section>
            </div>
          </div>
          
          <!--bg_content--> 
          
        </figure>
      </div>
      
      <!--bg_slid_part--> 
      
    </div>
    
    <!--bg_slide--> 
    
  </div>
  
  <!--headerpatent--> 
  
</div>

<!-- /wrapper --> 

<!--JS--> 
<?php
endwhile;
get_footer();
?>
