<?php
/**
 * Template name: Front 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
    // Start the loop.
   while ( have_posts() ) : the_post();
   ?>
<style type="text/css">
  .lsh-plaza-block {
      z-index: 3;
  }
  .outre_circle {
    overflow: hidden;
  } 
  .outre_circle:after {
      content: '';
      position: absolute;
      top: 0px;
	  background: url(<?php bloginfo('template_url');?>/assets/images/k1.png) no-repeat center center;
      width: 100%;
      height: 100%;
      background-size: cover;
      z-index: -1;
  }
  .fs-lg.banner-img:nth-child(2):after, .fs-lg.banner-img:nth-child(3):before {
      background: none;
  }  
</style>
<div id="wrapper">
<?php echo lsh_header_output();?> 
  <!-- /header -->
  
  <section id="main">
    <div class="sidebar-nav">
      <ul id="menu">
        <li data-menuanchor="home" class="active"><a href="#home">&nbsp;</a></li>
        <li data-menuanchor="welcome"><a href="#welcome">&nbsp;</a></li>
        <li data-menuanchor="properties"><a href="#properties">&nbsp;</a></li>
        <li data-menuanchor="about-us"><a href="#about-us">&nbsp;</a></li>
        <li data-menuanchor="countries"><a href="#countries">&nbsp;</a></li>
        <li data-menuanchor="news"><a href="#news">&nbsp;</a></li> 
        <li data-menuanchor="about"><a href="#about">&nbsp;</a></li>
      </ul>
    </div>
    <div id="fullpage" class="all-sections" style="height: 100%; position: relative; transform: translate3d(0px, 0px, 0px); transition: all 700ms ease 0s;">

<?php 
$sliders = get_field('slider');
if( $sliders ):
?>

      <section class="section sec1 main-banner" id="section1">
        <div class="slide-images" id="banner-slider">
		<?php 
		foreach( $sliders as $nu => $slider_img ):
		$img_class = $nu == 0 ? 'active' : '';
		$slider_img_style = $slider_img['slider_picture'];
		 ?>
		  <figure class="fs-lg banner-img <?php echo $img_class; ?>" style="background-image:url('<?php echo $slider_img_style['sizes']['home_header_slider_img'];?>');">
          </figure>
		  <?php endforeach; ?>
        </div>

        <canvas id="progresscanvas" width="1280" height="767"></canvas>
        <canvas id="dottedcanvas" width="1280" height="767"></canvas>
        <canvas id="dotimagecanvas" width="1280" height="767"></canvas>
		<div class="contein-tabs">
          <div class="elite-property active"> 
            <div class="elite-text">
			<h2><?php the_field('main_text_big'); ?> <span><?php the_field('main_text_small');?></span></h2>
            </div>
          </div>
          <div class="elite-property"> 
            <div class="elite-text">
			<h2><?php the_field('main_text_big');?><span><?php the_field('main_text_small');?></span></h2>
            </div>
          </div>
          <div class="elite-property"> 
            <div class="elite-text">
			<h2><?php the_field('main_text_big');?> <span><?php the_field('main_text_small');?></span></h2>
            </div>
          </div>
        </div>

        <img id="dotactive" src="<?php bloginfo('template_url');?>/assets/images/dotactive.png" style="display: none;"> <img id="dotnormal" src="<?php bloginfo('template_url');?>/assets/images/dotnormal.png" style="display: none;">
        <div class="lsh-plaza-block">
          <div class="container clearfix">
            <div class="lsh-plaza">
			<?php 
			foreach($sliders as $or => $slider):
			$slider_class = $or == 0 ? 'active': '';
			$slider_link_id = $slider['link'] ? $slider['link']->ID : 142;
			?>
				<div class="plaza-img <?php echo $slider_class; ?>"><span><img src="<?php  echo $slider['slider_icon']['url']; ?>" alt="plaza"></span> <a href="<?php echo get_permalink($slider_link_id);?>"><?php echo $slider['slider_title']; ?></a> </div>
				<?php endforeach; ?>
            </div>
            <div class="chat-img"> <a href="#"><img src="<?php bloginfo('template_url');?>/assets/images/chat.png" alt="chat"></a>
              <div class="pop_up">
			  <img src="<?php bloginfo('template_url');?>/assets/images/WX20170620-095407@2x.png" alt="" class="qr__img">
              </div>
            </div>

            <div class="scroll-arrow" id="animate-scroll">
              <li data-menuanchor="welcome" class="active">
                <a href="#welcome">
                  <span class="top"></span>
                  <span class="bottom"></span>
                </a>
              </li>
            </div>
          </div>
        </div>
        
        <!--pop_up-->
        <div class="outre_circle"></div>
        <div class="outre_circle_mob"></div>
      </section>

<?php endif;?>

		<section class="section sec1 welcome-wrap" id="section2" style="background-image: url(<?php echo get_field('welcome_background_picture')['url'];?>)">
        <div class="container">
          <div class="welcome-to-lsh">
		  <h2><?php the_field('welcome_title_1');?><span><?php the_field('welcome_title_2');?></span></h2>
		  <?php the_field('welcome_text');?>
			<a href="<?php the_field('welcome_link');?>" class="btn"><span><?php the_field('welcome_link_title');?></span></a> </div>
        </div>
      </section>

<?php 
$services = get_field('service');
if( $services ):
?>
      <section class="section sec1 commercial-wrap" id="section3">
					<div class="commercial-tabs-block">
						<div class="container clearfix">
							<div class="commercial-tabs">
								<div class="commercial-tab-links">
									<ul>
									    <?php 
									    foreach( $services as $nu => $service_icon ): 
									     $service_icon_class = $nu == 0 ? 'active' : '#';
									     $service_icon_normal = $service_icon['service_icon'];
									     $service_icon_active = $service_icon['service_active_icon'];
									    ?>
										<li class="<?php echo $service_icon_class; ?>">
											<span class="before"></span>
											    <div class="icon_two">
												<img src="<?php echo $service_icon_active['url'];?>" alt="commercial" class="active-img">
												<img src="<?php echo $service_icon_normal['url']; ?>" alt="commercial" class="normal-img">
											    </div>
											<span class="after"></span>
										</li>
										<?php endforeach; ?>
									</ul>
								</div>
								<div class="commercial-tabs-content">

									    <?php 
									    foreach( $services as $nu => $service_tag): 
									     $service_tag_class = $nu == 0 ? 'active' : '';
									    ?>
										<div class="commercial-tab1 <?php echo $service_tag_class; ?>">
										<h3><?php echo $service_tag['service_title']; ?></h3>
										<div class="projects">
											<ul>
											<li><?php _e('Projects', 'lsh'); ?><strong><?php echo $service_tag['category_information']->count;?></strong></li>
											<li><?php _e('Area', 'lsh');  ?> <strong><?php echo lsh_total_area_of_a_cat($service_tag['category_information']->term_id);?>M<sup>2</sup></strong></li>
											</ul>
										</div>
										<div class="high-end">
										<a href="<?php echo $service_tag['service_link']?>"><span><img src="<?php bloginfo('template_url');?>/assets/images/arrow.png" alt="arrow"></span><?php echo $service_tag['service_link_title'];?></a>
										</div>
									</div>
									<?php endforeach; ?>	
								</div>
							</div>
						</div>
					</div>

					<div id="horizontal-slider">
						<?php 
						foreach( $services as $nu => $service_bg ): 
						 $service_bg_class = $nu == 0 ? 'active commercial-slider' : '';
						 $service_bg_img = $service_bg['service_picture'];
						?>
					    <div class="hor-slide-item fs-lg bg-cover <?php echo $service_bg_class;?>" style="background-image:url('<?php echo $service_bg_img['sizes']['home_header_slider_img']; ?>');"></div>
					    <?php endforeach; ?>
					</div>
				</section>
				<?php endif;?>

      <section class="section sec1 project-developement fullpage-section">
        <div class="our-developement">
		<h3><?php the_field('project_title'); ?></h3>	
		<?php the_field('project_text'); ?>	
        </div>
      </section>
      <section class="section sec1 china-overseas-wrap" id="section4">
        <div class="clearfix">
          <div class="china-block" id="china">
            <div class="china_alignment">
			<?php 
			$china = get_field('project_category_left');
			if( $china ){
				echo lsh_output_sub_list_of_category( $china);}
			?>
			</div>
          </div>
          <div class="china-block" id="overseas">
            <div class="china_alignment">
			<?php 
			$overseas = get_field('project_category_right');
			if( $overseas ){
			echo lsh_output_sub_list_of_category( $overseas );}
			?>
			</div>
          </div>
		  <div class="mob_btn"> <a class="btn mobile_btn" href="#"> <span><?php _e('Learn more', 'lsh'); ?></span> </a> </div>
          <!--mob_btn--> 
        </div>

		<?php 
		$china_term = get_term_by('term_id', $china, 'countries');
		$overseas_term = get_term_by('term_id', $overseas, 'countries');
		?>

        <div class="center-block active">
          <div class="china-text">
            <div class="center-text">
			<p><?php echo $china_term->description; ?> </p>
			<a href="#" class="btn"><span><?php _e('Learn more', 'lsh'); ?></span></a> 
            </div>
          </div>
          <div class="overseas-text">
            <div class="center-text">
				</p><?php echo $overseas_term->description; ?></p>
				<a href="#" class="btn"><span><?php _e('Learn more', 'lsh'); ?></span></a> 
            </div>
          </div>
        </div>
      </section>

	<?php
	$news_args = array (
		'post_type' => 'post',
		'post_status'    => 'publish',
		'posts_per_page' => 3,
	);  

	$news = new WP_Query( $news_args );
	if( $news->have_posts()):
	?>
      <section class="section sec1 latest-news-wrap fp-auto-height" id="section5">
        <div class="container">
		<h3><?php _e('Latest News', 'lsh'); ?></h3>
          <div class="latest-news">
			<?php while( $news->have_posts()):
				$news->the_post();
			?>
            <div class="news1">
              <div class="news-img">
				<figure><a href="<?php the_permalink();?>"><img src="<?php the_post_thumbnail_url('news_list_img'); ?>" alt="news1"></a></figure>
              </div>
              <div class="news-content">
			  <h5><a href="<?php the_permalink();?>"><?php the_title(); ?><span><?php echo get_the_date('Y-m-d'); ?></span></a></h5>
			  <?php the_excerpt(); ?>
              </div>
              <div class="news-date">
			  <p><?php echo get_the_date('Y M');?>. <strong><?php echo get_the_date('d'); ?></strong></p>
              </div>
            </div>
		<?php endwhile;  wp_reset_postdata(); ?>

          </div>
			  <a href="<?php echo get_permalink(302);?>" class="btn"><span><?php _e('Learn more', 'lsh'); ?></span></a> </div>
      </section>
	  <?php endif; ?>

	  <footer id="colophon" class="section fp-auto-height" style="background-image: url(<?php bloginfo('template_url');?>/assets/images/footer-bg.png);">

        <div class="container cfix">
			<?php echo lsh_footer_menu(); ?>	
          <!-- /footer_row -->
        </div>

		<?php echo lsh_copyright_output(); ?> 
      </footer>
      <!-- /footer --> 
    </div>
  </section>
  <!-- /main --> 
  
</div>
<?php 
endwhile;
get_footer();
?>
