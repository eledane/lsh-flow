<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage lsh 
 * @since 1.0
 * @version 1.0
 */

?>
<!--
<script>
window.jQuery || document.write('<script src="<?php bloginfo('template_url');?>/assets/js/vendor/jquery-3.1.0.min.js"><\/script>')
</script> 
-->
<script>

jQuery(document).ready(function(){


/*
jQuery(window).scroll(function() {    

    var scroll = jQuery(window).scrollTop();

if (scroll >= 500) {

	jQuery("nav, .t-symbol").addClass("change_color");

} else {

	jQuery("nav, .t-symbol").removeClass("change_color");

}

if (scroll >= 500) {

	jQuery(".logo .blue-logo").addClass("blue-logo1");

} else {

	jQuery(".logo .blue-logo").removeClass("blue-logo1");

}		

if(scroll >= 900){

 jQuery("nav, .t-symbol").addClass("change_color2");

} else {

	jQuery("nav, .t-symbol").removeClass("change_color2");

}

if (scroll >= 900) {

	jQuery(".logo .blue-logo").addClass("blue-logo2");

} else {

	jQuery(".logo .blue-logo").removeClass("blue-logo2");

}	 

if(scroll >= 1600){

 jQuery("nav, .t-symbol").addClass("change_color3");

} else {

	jQuery("nav, .t-symbol").removeClass("change_color3");

}

if (scroll >= 1600) {

	jQuery(".logo .blue-logo").addClass("blue-logo3");

} else {

	jQuery(".logo .blue-logo").removeClass("blue-logo3");

}

if(scroll >= 2000){

	jQuery("nav, .t-symbol").addClass("change_color5");

}

else {

	jQuery("nav, .t-symbol").removeClass("change_color5");

}

if (scroll >= 2000) {

	jQuery(".logo .blue-logo").addClass("blue-logo4");

} else {

	jQuery(".logo .blue-logo").removeClass("blue-logo4");

}

})

*/
    

});

jQuery(document).ready(function(){
    var scroll = jQuery(window).scrollTop();
    //console.log( scroll );
    if (scroll >= 20 ) {

        jQuery('#header').addClass('scrolled');
        jQuery("nav, .t-symbol").addClass("change_color");
        jQuery(".logo .blue-logo").addClass("blue-logo1");

    } else {
        jQuery('#header').removeClass('scrolled');
        jQuery("nav, .t-symbol").removeClass("change_color");
        jQuery(".logo .blue-logo").removeClass("blue-logo1");
    }
})

jQuery(window).scroll( function() {    
        
    var scroll = jQuery(window).scrollTop();
    //console.log( scroll );
    if (scroll >= 20 ) {

        jQuery('#header').addClass('scrolled');
        jQuery("nav, .t-symbol").addClass("change_color");
        jQuery(".logo .blue-logo").addClass("blue-logo1");

    } else {
        jQuery('#header').removeClass('scrolled');
        jQuery("nav, .t-symbol").removeClass("change_color");
        jQuery(".logo .blue-logo").removeClass("blue-logo1");
    }
});

</script>
<?php wp_footer(); ?>
</body>
</html>
